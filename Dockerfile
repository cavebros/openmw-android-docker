# syntax=docker/dockerfile:labs
FROM rockylinux:9

#Set build type : release, RelWithDebInfo, debug
ENV BUILD_TYPE=release

# App versions - change settings here
ARG LIBJPEG_TURBO_VERSION=3.0.2
ARG LIBPNG_VERSION=1.6.42
ARG FREETYPE2_VERSION=2.13.2
ARG OPENAL_VERSION=1.23.1
ARG BOOST_VERSION=1.86.0
ARG LIBICU_VERSION=70-1
ARG FFMPEG_VERSION=6.1
ARG SDL2_VERSION=2.30.7
ARG BULLET_VERSION=3.25
ARG ZLIB_VERSION=1.3.1
ARG LIBXML2_VERSION=2.12.5
ARG MYGUI_VERSION=3.4.3
ARG GL4ES_VERSION=3e9cee8122a99b60b0abea8878262e13810cb11c
ARG COLLADA_DOM_VERSION=2.5.0
ARG OSG_VERSION=3.6.5.1
ARG LZ4_VERSION=1.9.3
ARG LUAJIT_VERSION=2.1.ROLLING
ARG OPENMW_VERSION=fe047d98b144ade8b301543f3f199403fc26b232
ARG NDK_VERSION=27.2.12479018
ARG SDK_CMDLINE_TOOLS=10406996_latest
ARG JAVA_VERSION=21

# Version of Release
ARG APP_VERSION=Alpha

RUN dnf install -y dnf-plugins-core && dnf config-manager --set-enabled crb && dnf install -y epel-release
RUN dnf install -y https://mirrors.rpmfusion.org/free/el/rpmfusion-free-release-9.noarch.rpm \
    && dnf install -y xz p7zip bzip2 libstdc++-devel glibc-devel zip unzip libcurl-devel which wget python-devel doxygen nano gcc-c++ libxcb-devel git java-${JAVA_VERSION}-openjdk-devel cmake patch


RUN mkdir -p ${HOME}/prefix
RUN mkdir -p ${HOME}/src

# Set the installation Dir
ENV PREFIX=/root/prefix
RUN cd ${HOME}/src && wget https://github.com/unicode-org/icu/archive/refs/tags/release-${LIBICU_VERSION}.zip && unzip -o ${HOME}/src/release-${LIBICU_VERSION}.zip && rm -rf release-${LIBICU_VERSION}.zip
RUN wget https://dl.google.com/android/repository/commandlinetools-linux-${SDK_CMDLINE_TOOLS}.zip && unzip commandlinetools-linux-${SDK_CMDLINE_TOOLS}.zip && mkdir -p ${HOME}/Android/cmdline-tools/ && mv cmdline-tools/ ${HOME}/Android/cmdline-tools/latest && rm commandlinetools-linux-${SDK_CMDLINE_TOOLS}.zip


# Setup sdkmanager and all tools
ENV ANDROID_HOME=/root/Android
RUN yes | ~/Android/cmdline-tools/latest/bin/sdkmanager --licenses > /dev/null
RUN ~/Android/cmdline-tools/latest/bin/sdkmanager --install "ndk;${NDK_VERSION}" "platforms;android-34" "platform-tools" "build-tools;34.0.0" --channel=0
RUN yes | ~/Android/cmdline-tools/latest/bin/sdkmanager --licenses > /dev/null

#Setup ICU for the Host
RUN mkdir -p ${HOME}/src/icu-host-build && cd $_ && ${HOME}/src/icu-release-70-1/icu4c/source/configure --disable-tests --disable-samples --disable-icuio --disable-extras CC="gcc" CXX="g++" && make -j $(nproc)
ENV PATH=$PATH:/root/Android/cmdline-tools/latest/bin/:/root/Android/ndk/${NDK_VERSION}/:/root/Android/ndk/${NDK_VERSION}/toolchains/llvm/prebuilt/linux-x86_64:/root/Android/ndk/${NDK_VERSION}/toolchains/llvm/prebuilt/linux-x86_64/bin:/root/prefix/include:/root/prefix/lib:/root/prefix/:/root/.cargo/bin

# NDK Settings
ENV API=24
ENV ABI=arm64-v8a
ENV ARCH=aarch64
ENV NDK_TRIPLET=${ARCH}-linux-android
ENV TOOLCHAIN=/root/Android/ndk/${NDK_VERSION}/toolchains/llvm/prebuilt/linux-x86_64
ENV NDK_SYSROOT=${TOOLCHAIN}/sysroot/
ENV ANDROID_SYSROOT=${TOOLCHAIN}/sysroot/
 # ANDROID_NDK is needed for SDL2 cmake
ENV ANDROID_NDK=/root/Android/ndk/${NDK_VERSION}/
ENV AR=${TOOLCHAIN}/bin/llvm-ar
ENV LD=${TOOLCHAIN}/bin/ld
ENV RANLIB=${TOOLCHAIN}/bin/llvm-ranlib
ENV STRIP=${TOOLCHAIN}/bin/llvm-strip
ENV CC=${TOOLCHAIN}/bin/${NDK_TRIPLET}${API}-clang
ENV CXX=${TOOLCHAIN}/bin/${NDK_TRIPLET}${API}-clang++
ENV clang=${TOOLCHAIN}/bin/${NDK_TRIPLET}${API}-clang
ENV clang++=${TOOLCHAIN}/bin/${NDK_TRIPLET}${API}-clang++
ENV PKG_CONFIG_LIBDIR=${PREFIX}/lib/pkgconfig

# Global C, CXX and LDFLAGS
ENV CFLAGS="-fPIC -O3 -flto=thin"
ENV CXXFLAGS="-fPIC -O3 -frtti -fexceptions -flto=thin"
ENV LDFLAGS="-fPIC -Wl,--undefined-version -flto=thin -fuse-ld=lld"

ENV COMMON_CMAKE_ARGS \
  "-DCMAKE_TOOLCHAIN_FILE=/root/Android/ndk/${NDK_VERSION}/build/cmake/android.toolchain.cmake" \
  "-DANDROID_ABI=${ABI}" \
  "-DANDROID_PLATFORM=${API}" \
  "-DANDROID_STL=c++_shared" \
  "-DANDROID_CPP_FEATURES=" \
  "-DANDROID_ALLOW_UNDEFINED_VERSION_SCRIPT_SYMBOLS=ON" \
  "-DCMAKE_BUILD_TYPE=$BUILD_TYPE" \
  "-DCMAKE_C_FLAGS=-I${PREFIX}" \
  "-DCMAKE_DEBUG_POSTFIX=" \
  "-DCMAKE_INSTALL_PREFIX=${PREFIX}" \
  "-DCMAKE_FIND_ROOT_PATH=${PREFIX}" \
  "-DCMAKE_CXX_COMPILER=${NDK_TRIPLET}${API}-clang++" \
  "-DCMAKE_CC_COMPILER=${NDK_TRIPLET}${API}-clang" \
  "-DHAVE_LD_VERSION_SCRIPT=OFF"

ENV COMMON_AUTOCONF_FLAGS="--enable-static --disable-shared --prefix=${PREFIX} --host=${NDK_TRIPLET}${API}"

ENV NDK_BUILD_FLAGS \
    "NDK_PROJECT_PATH=." \
    "APP_BUILD_SCRIPT=./Android.mk" \
    "APP_PLATFORM=${API}" \
    "APP_ABI=${ABI}"

# Setup rust build system for android
RUN wget https://sh.rustup.rs -O rustup.sh && sha256sum rustup.sh && \
    echo "b25b33de9e5678e976905db7f21b42a58fb124dd098b35a962f963734b790a9b  rustup.sh" | sha256sum -c - && \
    sh rustup.sh -y && rm rustup.sh && \
    ${HOME}/.cargo/bin/rustup target add ${NDK_TRIPLET} && \
    ${HOME}/.cargo/bin/rustup toolchain install nightly && \
    ${HOME}/.cargo/bin/rustup target add --toolchain nightly ${NDK_TRIPLET} && \
    echo "[target.${NDK_TRIPLET}]" >> /root/.cargo/config && \
    echo "linker = \"${TOOLCHAIN}/bin/${NDK_TRIPLET}${API}-clang\"" >> /root/.cargo/config

# Setup LIBICU
RUN mkdir -p ${HOME}/src/icu-${LIBICU_VERSION} && cd $_ && \
    ${HOME}/src/icu-release-${LIBICU_VERSION}/icu4c/source/configure \
        ${COMMON_AUTOCONF_FLAGS} \
        --disable-tests \
        --disable-samples \
        --disable-icuio \
        --disable-extras \
        --prefix=${PREFIX} \
        --with-cross-build=/root/src/icu-host-build && \
    make -j $(nproc) check_PROGRAMS= bin_PROGRAMS= && \
    make install check_PROGRAMS= bin_PROGRAMS=

# Setup Bzip2
RUN cd $HOME/src/ && git clone https://github.com/libarchive/bzip2 && cd bzip2 && \
    cmake . \
        $COMMON_CMAKE_ARGS && \
    make -j $(nproc) && make install

# Setup ZLIB
RUN wget -c https://github.com/madler/zlib/archive/refs/tags/v${ZLIB_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/zlib-${ZLIB_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} && \
    make -j $(nproc) && make install

# Setup LIBJPEG_TURBO
RUN wget -c https://github.com/libjpeg-turbo/libjpeg-turbo/releases/download/${LIBJPEG_TURBO_VERSION}/libjpeg-turbo-${LIBJPEG_TURBO_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/libjpeg-turbo-${LIBJPEG_TURBO_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} \
        -DENABLE_SHARED=false && \
    make -j $(nproc) && make install

# Setup LIBPNG
RUN wget -c http://prdownloads.sourceforge.net/libpng/libpng-${LIBPNG_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/libpng-${LIBPNG_VERSION}/build && cd $_ && \
        ${HOME}/src/libpng-${LIBPNG_VERSION}/configure \
        ${COMMON_AUTOCONF_FLAGS} && \
    make -j $(nproc) check_PROGRAMS= bin_PROGRAMS= && \
    make install check_PROGRAMS= bin_PROGRAMS=

# Setup FREETYPE2
RUN wget -c http://prdownloads.sourceforge.net/freetype/freetype-${FREETYPE2_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/freetype-${FREETYPE2_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} \
        -DCMAKE_DISABLE_FIND_PACKAGE_ZLIB=TRUE \
        -DCMAKE_DISABLE_FIND_PACKAGE_BZip2=TRUE \
        -DCMAKE_DISABLE_FIND_PACKAGE_PNG=TRUE && \
    make -j $(nproc) && make install

# Setup LIBXML
RUN wget -c https://github.com/GNOME/libxml2/archive/refs/tags/v${LIBXML2_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/libxml2-${LIBXML2_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} \
        -DBUILD_SHARED_LIBS=OFF \
        -DLIBXML2_WITH_THREADS=ON \
        -DLIBXML2_WITH_CATALOG=OFF \
        -DLIBXML2_WITH_ICONV=OFF \
        -DLIBXML2_WITH_LZMA=OFF \
        -DLIBXML2_WITH_PROGRAMS=OFF \
        -DLIBXML2_WITH_PYTHON=OFF \
        -DLIBXML2_WITH_TESTS=OFF \
        -DLIBXML2_WITH_ZLIB=ON && \
    make -j $(nproc) && make install

# Setup OPENAL
RUN wget -c https://github.com/kcat/openal-soft/archive/${OPENAL_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/openal-soft-${OPENAL_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} \
        -DALSOFT_EXAMPLES=OFF \
        -DALSOFT_TESTS=OFF \
        -DALSOFT_UTILS=OFF \
        -DALSOFT_NO_CONFIG_UTIL=ON \
        -DALSOFT_BACKEND_OPENSL=ON \
        -DALSOFT_BACKEND_WAVE=OFF && \
    make -j $(nproc) && make install

COPY --chmod=0755 patches /root/patches

# Setup BOOST
RUN wget -c https://github.com/boostorg/boost/releases/download/boost-${BOOST_VERSION}/boost-${BOOST_VERSION}-cmake.tar.gz -O - | tar -xz -C $HOME/src/ && \
    patch -d ${HOME}/src/boost-${BOOST_VERSION}/libs/system/ -p1 -t -N < /root/patches/openmw/system.diff && \
    patch -d ${HOME}/src/boost-${BOOST_VERSION}/libs/regex/ -p1 -t -N < /root/patches/openmw/regex.diff && \
    mkdir -p ${HOME}/src/boost-${BOOST_VERSION}/build && cd $_ && \
    cmake ../ ${COMMON_CMAKE_ARGS} \
        -DCMAKE_CXX_FLAGS=-I${HOME}/src/boost-${BOOST_VERSION}/libs/core/include/\ "${CXXFLAGS}" \
        -DBOOST_INCLUDE_LIBRARIES="filesystem;program_options;iostreams;geometry;system;regex" && \
    make -j $(nproc) && make install && $RANLIB ${PREFIX}/lib/libboost_*.a

# Setup FFMPEG_VERSION
RUN wget -c http://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.bz2 -O - | tar -xjf - -C ${HOME}/src/ && \
    mkdir -p ${HOME}/src/ffmpeg-${FFMPEG_VERSION} && cd $_ && \
    ${HOME}/src/ffmpeg-${FFMPEG_VERSION}/configure \
        --disable-asm \
        --disable-optimizations \
        --target-os=android \
        --enable-cross-compile \
        --cross-prefix=${TOOLCHAIN}/bin/llvm- \
        --cc=${NDK_TRIPLET}${API}-clang \
        --arch=arm64 \
        --cpu=armv8-a \
        --prefix=${PREFIX} \
        --enable-version3 \
        --enable-pic \
        --disable-everything \
        --disable-doc \
        --disable-programs \
        --disable-autodetect \
        --disable-iconv \
        --enable-decoder=mp3 \
        --enable-demuxer=mp3 \
        --enable-decoder=bink \
        --enable-decoder=binkaudio_rdft \
        --enable-decoder=binkaudio_dct \
        --enable-demuxer=bink \
        --enable-demuxer=wav \
        --enable-decoder=pcm_* \
        --enable-decoder=vp8 \
        --enable-decoder=vp9 \
        --enable-decoder=opus \
        --enable-decoder=vorbis \
        --enable-demuxer=matroska \
        --enable-demuxer=ogg && \
    make -j $(nproc) && make install

# Setup SDL2_VERSION
RUN wget -c https://github.com/libsdl-org/SDL/releases/download/release-${SDL2_VERSION}/SDL2-${SDL2_VERSION}.tar.gz -O - | tar -xz -C ${HOME}/src/ && \
    mkdir -p ${HOME}/src/SDL2-${SDL2_VERSION}/build && cd $_ && \
    cmake ../ ${COMMON_CMAKE_ARGS} \
        -DSDL_STATIC=OFF \
        -DCMAKE_C_FLAGS=-DHAVE_GCC_FVISIBILITY=OFF\ "${CFLAGS}" && \
    make -j $(nproc) && make install
RUN cp -rf ${HOME}/src/SDL2-${SDL2_VERSION}/include/* /root/prefix/include/

# Setup BULLET
RUN wget -c https://github.com/bulletphysics/bullet3/archive/${BULLET_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/bullet3-${BULLET_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} \
        -DBUILD_BULLET2_DEMOS=OFF \
        -DBUILD_CPU_DEMOS=OFF \
        -DBUILD_UNIT_TESTS=OFF \
        -DBUILD_EXTRAS=OFF \
        -DUSE_DOUBLE_PRECISION=ON \
        -DBULLET2_MULTITHREADING=ON && \
    make -j $(nproc) && make install

# Setup GL4ES_VERSION
RUN wget -c https://github.com/Duron27/gl4es/archive/${GL4ES_VERSION}.tar.gz -O - | tar -xz -C ${HOME}/src/ && \
    mkdir -p ${HOME}/src/gl4es-${GL4ES_VERSION}/build && cd $_ && \
    cmake ../ ${COMMON_CMAKE_ARGS} && \
    make -j $(nproc) && make install

# Setup MYGUI
RUN wget -c https://github.com/MyGUI/mygui/archive/MyGUI${MYGUI_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/mygui-MyGUI${MYGUI_VERSION}/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} \
        -DMYGUI_RENDERSYSTEM=1 \
        -DMYGUI_BUILD_DEMOS=OFF \
        -DMYGUI_BUILD_TOOLS=OFF \
        -DMYGUI_BUILD_PLUGINS=OFF \
        -DMYGUI_DONT_USE_OBSOLETE=ON \
        -DMYGUI_STATIC=ON && \
    make -j $(nproc) && make install

# Setup LZ4
RUN wget -c https://github.com/lz4/lz4/archive/v${LZ4_VERSION}.tar.gz -O - | tar -xz -C $HOME/src/ && \
    mkdir -p ${HOME}/src/lz4-${LZ4_VERSION}/build && cd $_ && \
    cmake cmake/ \
        ${COMMON_CMAKE_ARGS} \
        -DBUILD_STATIC_LIBS=ON \
        -DBUILD_SHARED_LIBS=OFF && \
    make -j $(nproc) && make install

# Setup LUAJIT_VERSION
RUN wget -c https://github.com/luaJit/LuaJIT/archive/v${LUAJIT_VERSION}.tar.gz -O - | tar -xz -C ${HOME}/src/ && \
    cd ${HOME}/src/LuaJIT-${LUAJIT_VERSION} && \
    make amalg \
    HOST_CC='gcc -m64' \
    CFLAGS= \
    TARGET_CFLAGS="${CFLAGS}" \
    PREFIX=${PREFIX} \
    CROSS=${TOOLCHAIN}/bin/llvm- \
    STATIC_CC=${NDK_TRIPLET}${API}-clang \
    DYNAMIC_CC="${NDK_TRIPLET}${API}-clang -fPIC" \
    TARGET_LD=${NDK_TRIPLET}${API}-clang && \
    make install \
    HOST_CC='gcc -m64' \
    CFLAGS= \
    TARGET_CFLAGS="${CFLAGS}" \
    PREFIX=${PREFIX} \
    CROSS=${TOOLCHAIN}/bin/llvm- \
    STATIC_CC=${NDK_TRIPLET}${API}-clang \
    DYNAMIC_CC="${NDK_TRIPLET}${API}-clang -fPIC" \
    TARGET_LD=${NDK_TRIPLET}${API}-clang

RUN rm ${PREFIX}/lib/libluajit*.so*

# Begin Star Control 2 Port

# Setup Libogg
RUN wget -c https://github.com/xiph/ogg/releases/download/v1.3.5/libogg-1.3.5.tar.gz -O - | tar -xz -C ${HOME}/src/ && cd ${HOME}/src/libogg-1.3.5 && \
    mkdir -p ${HOME}/src/libogg-1.3.5/build && cd $_ && \
    cmake .. \
        ${COMMON_CMAKE_ARGS} && \
    make -j $(nproc) && make install

# Setup Vorbis
RUN wget -c https://github.com/xiph/vorbis/releases/download/v1.3.7/libvorbis-1.3.7.tar.gz -O - | tar -xz -C ${HOME}/src/ && cd ${HOME}/src/libvorbis-1.3.7 && \
    mkdir -p ${HOME}/src/libvorbis-1.3.7/build && cd $_ && \
    cmake .. \
        ${COMMON_CMAKE_ARGS} && \
    make -j $(nproc) && make install

# Setup SDL_image
RUN wget -c https://github.com/libsdl-org/SDL_image/releases/download/release-2.8.4/SDL2_image-2.8.4.tar.gz -O - | tar -xz -C ${HOME}/src/ && cd ${HOME}/src/SDL2_image-2.8.4 && \
    mkdir -p ${HOME}/src/SDL2_image-2.8.4/build && cd $_ && \
    cmake .. \
        ${COMMON_CMAKE_ARGS} && \
    make -j $(nproc) && make install

RUN cd /root/src && git clone https://github.com/Duron27/sc2-uqm.git && \
    mkdir sc2-uqm/build && cd $_ && \
    cmake ../ \
        ${COMMON_CMAKE_ARGS} && \
    make -j $(nproc)

# End Port

# Setup LIBCOLLADA_VERSION
# The 3 sed commands are required for boost 1.85.0
RUN wget -c https://github.com/rdiankov/collada-dom/archive/v${COLLADA_DOM_VERSION}.tar.gz -O - | tar -xz -C ${HOME}/src/ && cd ${HOME}/src/collada-dom-${COLLADA_DOM_VERSION} && \
    cd ${HOME}/src/collada-dom-${COLLADA_DOM_VERSION} && \
    sed -i 's|#include <boost/filesystem/convenience.hpp>|#include <boost/filesystem.hpp>|g' dom/include/dae.h && \
    sed -i 's|#include <boost/filesystem/convenience.hpp>|#include <boost/filesystem.hpp>|g' dom/src/dae/daeUtils.cpp && \
    sed -i 's|std::string dir = archivePath.branch_path().string();|std::string dir = archivePath.parent_path().string();|g' dom/src/dae/daeZAEUncompressHandler.cpp && \
    mkdir -p ${HOME}/src/collada-dom-${COLLADA_DOM_VERSION}/build && cd $_ && \
    cmake .. \
        ${COMMON_CMAKE_ARGS} \
        -DBoost_USE_STATIC_LIBS=ON \
        -DBoost_USE_STATIC_RUNTIME=ON \
        -DBoost_NO_SYSTEM_PATHS=ON \
        -DBoost_INCLUDE_DIR=${PREFIX}/include \
        -DCMAKE_CXX_FLAGS=-Dauto_ptr=unique_ptr\ "${CXXFLAGS}" && \
    make -j $(nproc) && make install

# Setup Delta Plugin
RUN cd root/src && git clone https://gitlab.com/bmwinger/delta-plugin && cd delta-plugin && cargo build --target ${NDK_TRIPLET} --release
RUN cp /root/src/delta-plugin/target/${NDK_TRIPLET}/release/delta_plugin ${PREFIX}/lib/libdelta_plugin.so

# Setup S3LightFixes
RUN cd root/src && git clone https://github.com/magicaldave/S3LightFixes && cd S3LightFixes && cargo build --target ${NDK_TRIPLET} --release
RUN cp /root/src/S3LightFixes/target/${NDK_TRIPLET}/release/s3lightfixes ${PREFIX}/lib/libS3LightFixes.so

# Install QT for android
#RUN pip install aqtinstall
#RUN aqt install-qt linux android 5.15.2 android --autodesktop
#RUN cp -r /5.15.2/android/* ${PREFIX} && rm -rf /5.15.2

# Setup OPENSCENEGRAPH_VERSION
RUN wget -c https://github.com/Duron27/osg/archive/refs/tags/${OSG_VERSION}.tar.gz -O - | tar -xz -C ${HOME}/src/ && \
    mkdir -p ${HOME}/src/osg-${OSG_VERSION}/build && cd $_ && \
    cmake .. \
        ${COMMON_CMAKE_ARGS} \
        -DOPENGL_PROFILE=GL1 \
        -DDYNAMIC_OPENTHREADS=OFF \
        -DDYNAMIC_OPENSCENEGRAPH=OFF \
        -DBUILD_OSG_PLUGIN_OSG=ON \
        -DBUILD_OSG_PLUGIN_DAE=ON \
        -DBUILD_OSG_PLUGIN_DDS=ON \
        -DBUILD_OSG_PLUGIN_TGA=ON \
        -DBUILD_OSG_PLUGIN_BMP=ON \
        -DBUILD_OSG_PLUGIN_JPEG=ON \
        -DBUILD_OSG_PLUGIN_PNG=ON \
        -DBUILD_OSG_PLUGIN_FREETYPE=ON \
        -DOSG_CPP_EXCEPTIONS_AVAILABLE=TRUE \
        -DJPEG_INCLUDE_DIR=${PREFIX}/include/ \
        -DPNG_INCLUDE_DIR=${PREFIX}/include/ \
        -DCOLLADA_INCLUDE_DIR=${PREFIX}/include/collada-dom2.5 \
        -DOSG_GL1_AVAILABLE=ON \
        -DOSG_GL2_AVAILABLE=OFF \
        -DOSG_GL3_AVAILABLE=OFF \
        -DOSG_GLES1_AVAILABLE=OFF \
        -DOSG_GLES2_AVAILABLE=OFF \
        -DOSG_GL_LIBRARY_STATIC=OFF \
        -DOSG_GL_DISPLAYLISTS_AVAILABLE=OFF \
        -DOSG_GL_MATRICES_AVAILABLE=ON \
        -DOSG_GL_VERTEX_FUNCS_AVAILABLE=ON \
        -DOSG_GL_VERTEX_ARRAY_FUNCS_AVAILABLE=ON \
        -DOSG_GL_FIXED_FUNCTION_AVAILABLE=ON \
        -DBUILD_OSG_APPLICATIONS=OFF \
        -DBUILD_OSG_PLUGINS_BY_DEFAULT=OFF \
        -DBUILD_OSG_DEPRECATED_SERIALIZERS=OFF \
        -DOSG_FIND_3RD_PARTY_DEPS=OFF \
        -DOPENGL_INCLUDE_DIR=${PREFIX}/include/gl4es/ \
        -DCMAKE_CXX_FLAGS=-Dauto_ptr=unique_ptr\ -I${PREFIX}/include/freetype2/\ "${CXXFLAGS}" && \
    make -j $(nproc) && make install

# Create a zip of all the libraries
#RUN cd /root/prefix && zip -r /openmw-android-deps.zip ./*

# Setup OPENMW_VERSION (pick one, COPY or RUN wget)
#ARG OPENMW_VERSION=3e804042c42312a5463e259c68f62e658424bc8a

# For testing changes to openmw itself just clone openmw into the same folder dockerfile is in.
#COPY --chmod=0755 openmw /root/src/openmw-${OPENMW_VERSION}

# Or you can enable this
RUN cd /root/src && git clone https://github.com/OpenMW/openmw

RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/4367.diff
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/androidPath.patch
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/shaders.patch
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/context_fix.patch
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/navmesh.patch
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/4307.diff
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/4335.diff
RUN patch -d ${HOME}/src/openmw -p1 -t -N < /root/patches/openmw/fix_shadows.patch
RUN cp /root/patches/openmw/android_main.cpp /root/src/openmw/apps/openmw/android_main.cpp

# sed commands
# change post processing window size for android
RUN sed -i 's/600 600/600 400/g' ${HOME}/src/openmw/files/data/mygui/openmw_postprocessor_hud.layout

# Snells Window
RUN sed -i 's/float ior = (cameraPos.z>0.0)?(1.333\/1.0):(1.0\/1.333);/float ior = 1.333;/g' ${HOME}/src/openmw/files/shaders/compatibility/water.frag

RUN mkdir -p ${HOME}/src/openmw/build && cd $_ && \
    cmake .. \
        ${COMMON_CMAKE_ARGS} \
        -DBUILD_BSATOOL=0 \
        -DBUILD_NIFTEST=0 \
        -DBUILD_ESMTOOL=0 \
        -DBUILD_LAUNCHER=0 \
        -DBUILD_MWINIIMPORTER=0 \
        -DBUILD_ESSIMPORTER=0 \
        -DBUILD_OPENCS=0 \
        -DBUILD_NAVMESHTOOL=0 \
        -DBUILD_WIZARD=0 \
        -DBUILD_MYGUI_PLUGIN=0 \
        -DBUILD_BULLETOBJECTTOOL=0 \
        -DOPENMW_USE_SYSTEM_SQLITE3=OFF \
        -DOPENMW_USE_SYSTEM_YAML_CPP=OFF \
        -DOPENMW_USE_SYSTEM_ICU=ON \
        -DOPENAL_INCLUDE_DIR=${PREFIX}/include/AL/ \
        -DBullet_INCLUDE_DIR=${PREFIX}/include/bullet/ \
        -DOSG_STATIC=TRUE \
        -DCMAKE_CXX_FLAGS=-std=gnu++20\ -I${PREFIX}/include/\ "${CXXFLAGS}" \
        -DMyGUI_LIBRARY=${PREFIX}/lib/libMyGUIEngineStatic.a && \
    make -j $(nproc)

COPY --chmod=0755 payload /root/payload
COPY --chmod=0755 mods /root/mods

# Prepare Launcher
RUN mkdir -p /root/payload/app/src/main/jniLibs/${ABI}/

# libopenmw.so and libopenmw-navmeshtool.so are special cases
RUN cp /root/src/openmw/build/libopenmw.so /root/payload/app/src/main/jniLibs/${ABI}/

# libuqm.so is a special cases
RUN cp /root/src/sc2-uqm/build/libuqm.so /root/payload/app/src/main/jniLibs/${ABI}/

# copy over libs we compiled
RUN cp ${PREFIX}/lib/lib{openal,SDL2,GL,collada-dom2.5-dp,delta_plugin,S3LightFixes}.so /root/payload/app/src/main/jniLibs/${ABI}/

# copy over libc++_shared
RUN find ${TOOLCHAIN}/sysroot/usr/lib/${NDK_TRIPLET} -iname "libc++_shared.so" -exec cp "{}" /root/payload/app/src/main/jniLibs/${ABI}/ \;
ENV DST=/root/payload/app/src/main/assets/libopenmw/
ENV SRC=/root/src/openmw/build/
RUN mkdir -p ${DST}/{openmw,resources,ui}

# Copy over Resources
RUN cp -r "${SRC}/resources" "${DST}"

# Copy over Mods
RUN cd ${DST}/resources/vfs/ && cp -r /root/mods/* .

# Copy over UI
RUN cp -r "${HOME}/payload/app/ui" "${DST}"

# Global Config
RUN cp "${SRC}/defaults.bin" "${DST}/openmw/"
RUN cp "${SRC}/gamecontrollerdb.txt" "${DST}/openmw/"
RUN cp "${HOME}/payload/app/settings.fallback.cfg" "${DST}/openmw/"
RUN cp "${HOME}/payload/app/openmw.cfg" "${DST}/openmw/"
RUN cp "${HOME}/payload/app/openmw.fallback.cfg" "${DST}/openmw/"
RUN cat "${SRC}/openmw.cfg" | grep -v "data=" | grep -v "data-local=" >> "${DST}/openmw/openmw.base.cfg"
RUN cat "/root/payload/app/openmw.base.cfg" >> "${DST}/openmw/openmw.base.cfg"
RUN echo "${APP_VERSION}" >> ${DST}/resources/version
#RUN sed -i "4i\    <string name='version_info'>CaveBros Version ${APP_VERSION}-${OPENMW_VERSION}</string>" /root/payload/app/src/main/res/values/strings.xml
RUN sed -i "s/ndkVersion = \".*\"/ndkVersion = \"${NDK_VERSION}\"/g" /root/payload/app/build.gradle.kts

# licensing info
RUN cp "/root/payload/3rdparty-licenses.txt" "${DST}"

# Remove Debug Symbols
RUN llvm-strip /root/payload/app/src/main/jniLibs/arm64-v8a/*.so

# Create Package for external Editing
RUN zip -r New_Launcher.zip /root/payload

# Build the APK!
RUN cd /root/payload/ && ./gradlew assembleDebug

RUN cp /root/payload/app/build/outputs/apk/debug/*.apk openmw-android.apk

**Cavebros 1.9 and up is located in the Legacy branch, all old launcher updates will happen there!**

If you have any issues find me on discord at @Duron27.

Using shaders from:
-   https://github.com/zesterer/openmw-shaders
-   https://gitlab.com/glassmancody.info/omwfx-shaders

Includes:
-   https://gitlab.com/bmwinger/delta-plugin/-/tree/master
-   https://github.com/rfuzzo/plox

USE AT YOUR OWN RISK!

# Building OpenMW for Android in a docker!

-   To build the docker use the command "sudo docker build -t dockerfile . --progress=plain"
-   To run it use "sudo docker run -it dockerfile"
-   The apk will be at the root directory

# Things I plan to add in the future
-   PLOX - load order sorter
-   TES3MP - this is a dream...maybe way way beyond my skills
-   Some kind of cloud syncing for mod list and saves

This release will start at version 2.0-alpha

# Known bugs.

-   Check the issue tracker for latest bugs.


**If you discover any bugs or issues not listed here please reach out on discord @Duron27, or leave an issue here. new feature suggestions are also welcome!**



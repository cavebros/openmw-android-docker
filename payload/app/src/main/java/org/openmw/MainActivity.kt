package org.openmw

import android.content.Context
import android.hardware.input.InputManager
import android.os.Bundle
import android.os.Process
import android.view.InputDevice.SOURCE_GAMEPAD
import android.view.WindowManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import org.openmw.ui.controls.UIStateManager
import org.openmw.ui.theme.OpenMWTheme
import org.openmw.utils.BouncingBackground
import org.openmw.utils.CaptureCrash
import org.openmw.utils.CircularBackground
import org.openmw.utils.ConfigFileObserver
import org.openmw.utils.GameFilesPreferences
import org.openmw.utils.GameFilesPreferences.readCodeGroup
import org.openmw.utils.LogRepository
import org.openmw.utils.LogsBox
import org.openmw.utils.ModValue
import org.openmw.utils.MultiPathFileObserver
import org.openmw.utils.MyAlertDialog
import org.openmw.utils.NoneBackground
import org.openmw.utils.PermissionHelper
import org.openmw.utils.RotatingImageBackground
import org.openmw.utils.UserManageAssets
import org.openmw.utils.currentDeviceRealSize
import org.openmw.utils.initializePreferences
import org.openmw.utils.readModValues
import org.openmw.utils.updateResolutionInConfig

@InternalCoroutinesApi
@ExperimentalMaterial3Api
class MainActivity : ComponentActivity() {
    private val scope = CoroutineScope(Dispatchers.Main)
    private val uiScope = CoroutineScope(Dispatchers.Main)
    private lateinit var configFileObserver: ConfigFileObserver
    private lateinit var fileObserver: MultiPathFileObserver

    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @OptIn(DelicateCoroutinesApi::class)
    @ExperimentalFoundationApi
    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)

        PermissionHelper.getManageExternalStoragePermission(this@MainActivity)
        PermissionHelper.getTermuxPermission(this@MainActivity)
        UserManageAssets(applicationContext).onFirstLaunch()
        Thread.setDefaultUncaughtExceptionHandler(CaptureCrash())

        scope.launch {
            initializePreferences(this@MainActivity)
        }

        startObservingCodeGroup(this, uiScope)
        val configFilePath = Constants.SETTINGS_FILE
        configFileObserver = ConfigFileObserver(configFilePath)
        configFileObserver.startWatching()

        val windowManager = this@MainActivity.getSystemService(WINDOW_SERVICE) as WindowManager
        val (width, height) = windowManager.currentDeviceRealSize()

        hideSystemBars(this)

        setContent {
            var showDialog = remember { mutableStateOf(true) }
            val modValues = readModValues()
            val newFeatureEnabledChecked by GameFilesPreferences.loadNewFeatureEnabledState(this).collectAsState(initial = false)
            val avoidInsertion by GameFilesPreferences.readResolutionInsertion(this).collectAsState(initial = false)
            val whatsNew by GameFilesPreferences.getWhatsNew(this).collectAsState(initial = false)

            if (newFeatureEnabledChecked) {
                val pathsToWatch = listOf(
                    Constants.USER_FILE_STORAGE,
                    this.filesDir.absolutePath,
                    this.filesDir.parentFile.absolutePath,
                    Constants.SECOND_USER_FILE_STORAGE,
                    this.applicationInfo.nativeLibraryDir
                )

                fileObserver = MultiPathFileObserver(pathsToWatch)
                fileObserver.startWatching()
            }

            if (!avoidInsertion) {
                updateResolutionInConfig(width, height)
            }
            OpenMWTheme {
                Surface(modifier = Modifier.fillMaxSize()) {
                    if (whatsNew) {
                        MyAlertDialog(showDialog = showDialog)
                    }
                    App(applicationContext, modValues)
                    if (UIStateManager.isAppLoggingEnabled) {
                        LogsBox(logs = LogRepository.logs, fontSize = 10f, boxWidth = 300f, boxHeight = 300f)
                    }
                }
            }
        }
    }

    public override fun onDestroy() {
        finish()
        configFileObserver.stopWatching()
        fileObserver.stopWatching()
        uiScope.cancel()
        super.onDestroy()
        Process.killProcess(Process.myPid())
    }
}

fun isControllerConnected(context: Context): Boolean {
    val inputManager = context.getSystemService(Context.INPUT_SERVICE) as InputManager
    val deviceIds = inputManager.inputDeviceIds
    for (id in deviceIds) {
        val device = inputManager.getInputDevice(id)
        if (device?.sources?.and(SOURCE_GAMEPAD) == SOURCE_GAMEPAD) {
            return true
        }
    }
    return false
}

fun startObservingCodeGroup(context: Context, scope: CoroutineScope) {
    scope.launch {
        readCodeGroup(context).collect { codeGroup ->
            UIStateManager.tempCodeGroup = codeGroup
        }
    }
}

@Composable
fun BackgroundAnimation() {
    var selectedBackgroundAnimation by remember { mutableStateOf("BouncingBackground") }
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current
    LaunchedEffect(Unit) {
        coroutineScope.launch {
            try {
                GameFilesPreferences.getBackgroundAnimation(context)?.let { option ->
                    selectedBackgroundAnimation = option
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    when (selectedBackgroundAnimation) {
        "BouncingBackground" -> BouncingBackground()
        "RotatingImageBackground" -> RotatingImageBackground()
        "CircularBackground" -> CircularBackground()
        else -> NoneBackground()
    }
}

@InternalCoroutinesApi
@DelicateCoroutinesApi
@ExperimentalFoundationApi
@ExperimentalMaterial3Api
@Composable
fun App(context: Context, modValues: List<ModValue>) {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screen.Home.route
    ) {
        composable(Screen.Setting.route) {
            SettingScreen(context) {
                navController.navigate(Screen.Home.route)
            }
        }
        composable(Screen.Home.route) {
            HomeScreen(context, modValues) {
                navController.navigate(Screen.Setting.route)
            }
        }
    }
}

private object Route {
    const val SETTINGS = "setting"
    const val HOME = "home"
}

sealed class Screen(val route: String) {
    object Setting: Screen(Route.SETTINGS)
    object Home: Screen(Route.HOME)
}


package org.openmw.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.Environment
import android.util.Log
import org.openmw.Constants
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.*

/**
 * Helper class to handle copying assets to the storage
 * @param context Android context to use
 */
class ManageAssets(val context: Context) {

    /**
     * Copies assets recursively
     * @param src Source directory in assets
     * @param dst Destination directory on disk, absolute path
     */
    @SuppressLint("SuspiciousIndentation")
    fun copy(src: String, dst: String) {
        val assetManager = context.assets
        try {
            val assets = assetManager.list(src) ?: return
            if (assets.isEmpty()) {
                copyFile(src, dst)
            } else {
                // Recurse into a subdirectory
                val dir = File(dst)
                if (!dir.exists())
                    dir.mkdirs()
                    for (i in assets.indices) {
                        copy(src + "/" + assets[i], dst + "/" + assets[i])
                    }
            }
        } catch (ex: IOException) {
            Log.e("ManageAssets", "Error copying assets from $src to $dst", ex)
        }
    }

    /**
     * Copies a single file from assets to disk
     * @param src Path of source file inside assets
     * @param dst Absolute path to destination file on disk
     */
    fun copyFile(src: String, dst: String) {
        try {
            val inp = context.assets.open(src)
            val out = FileOutputStream(dst)

            inp.copyTo(out)
            out.flush()

            inp.close()
            out.close()

            Log.d("ManageAssets", "Copied file from $src to $dst")
        } catch (e: IOException) {
            Log.e("ManageAssets", "Error copying file from $src to $dst", e)
        }
    }
}

class UserManageAssets(val context: Context) {

    val assetCopier = ManageAssets(context)

    @SuppressLint("SuspiciousIndentation")
    fun deleteRecursive(fileOrDirectory: File) {
        if (fileOrDirectory.isDirectory)
            for (child in fileOrDirectory.listFiles()!!)
                deleteRecursive(child)

                fileOrDirectory.delete()
    }

    /**
     * Removes old and creates new files located in private application directories
     * (i.e. under getFilesDir(), or /data/data/.../files)
     */
    fun reinstallStaticFiles() {
        // wipe old version first
        removeStaticFiles()

        Log.d("ManageAssets", "Copying resources to ${Constants.RESOURCES}")
        assetCopier.copy("libopenmw/resources", Constants.RESOURCES)

        Log.d("ManageAssets", "Copying global config to ${Constants.GLOBAL_CONFIG}")
        assetCopier.copy("libopenmw/openmw", Constants.GLOBAL_CONFIG)

        Log.d("ManageAssets", "Copying resources to ${Constants.UI}")
        assetCopier.copy("libopenmw/ui", Constants.UI)

        // set up user config (if not present)
        File(Constants.USER_CONFIG).mkdirs()
        if (!File(Constants.USER_OPENMW_CFG).exists()) {
            File(Constants.USER_OPENMW_CFG).writeText("# This is the user openmw.cfg. Feel free to modify it as you wish.\n")
        }
    }

    /**
     * Reset User Config
     */
    fun resetUserConfig() {
        // Check if the file exists and delete it if it does
        val settingsFile = File(Constants.SETTINGS_FILE)
        if (settingsFile.exists()) {
            Log.d("ManageAssets", "Deleting existing file: ${Constants.SETTINGS_FILE}")
            settingsFile.delete()
        }

        // Copy over settings.cfg
        Log.d("ManageAssets", "Copying settings.cfg to ${Constants.SETTINGS_FILE}")
        assetCopier.copy("libopenmw/openmw/settings.fallback.cfg", Constants.SETTINGS_FILE)
    }


    /**
     * Removes global static files, these include resources and config
     */
    fun removeStaticFiles() {
        // remove version stamp so that reinstallStaticFiles is called during game launch
        File(Constants.VERSION_STAMP).delete()

        deleteRecursive(File(Constants.GLOBAL_CONFIG))
        deleteRecursive(File(Constants.RESOURCES))
    }

    fun createFoldersInDownloads(): List<String> {
        val downloadDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        val openMwDir = File(downloadDir, "OpenMW")
        val folders = listOf(
            File(openMwDir, "Override"),
            File(openMwDir, "CACHE"),
            File(openMwDir, "Mods"),
            File(openMwDir, "Exports")
        )

        val createdFolders = mutableListOf<String>()

        if (!openMwDir.exists()) {
            if (openMwDir.mkdir()) {
                createdFolders.add(openMwDir.absolutePath)
            } else {
                println("Error: Could not create folder: ${openMwDir.absolutePath}")
            }
        }

        folders.forEach { folder ->
            if (!folder.exists()) {
                if (folder.mkdir()) {
                    createdFolders.add(folder.absolutePath)
                } else {
                    println("Error: Could not create folder: ${folder.absolutePath}")
                }
            } else {
                println("Folder already exists: ${folder.absolutePath}")
            }
        }

        return createdFolders
    }

    fun copyMissingFiles() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val srcDir = Paths.get(Constants.UI)
            val dstDir = Paths.get(Constants.USER_UI)

            Files.walk(srcDir).use { paths ->
                paths.filter { path -> Files.isRegularFile(path) }.forEach { srcFile ->
                    val relativePath = srcDir.relativize(srcFile)
                    val dstFile = dstDir.resolve(relativePath)

                    if (!Files.exists(dstFile) || Files.size(srcFile) != Files.size(dstFile)) {
                        Files.createDirectories(dstFile.parent)
                        Files.copy(srcFile, dstFile, StandardCopyOption.REPLACE_EXISTING)
                        println("Copied: $srcFile to $dstFile")
                    }
                }
            }
        }
    }

    /**
     * First launch
     */
    fun onFirstLaunch() {
        if (!File(Constants.USER_FILE_STORAGE + "/resources/").isDirectory) {
            val src = File(Constants.RESOURCES)
            val dst = File(Constants.USER_FILE_STORAGE + "/resources/")
            dst.mkdirs()
            src.copyRecursively(dst, true)
        }

        // copy fallback openmw_base.cfg
        if (!File(Constants.DEFAULTS_BIN).exists()) {
            reinstallStaticFiles()
        }

        createFoldersInDownloads()

        // set up user config (if not present)
        File(Constants.USER_CONFIG).mkdirs()
        if (!File(Constants.USER_OPENMW_CFG).exists()) {
            File(Constants.USER_OPENMW_CFG).writeText("# This is the user openmw.cfg. Feel free to modify it as you wish.\n")
        }

        if (!File(Constants.USER_DELTA).isDirectory) {
            File(Constants.USER_DELTA).mkdirs()
        }

        createUI()

        // copy user settings file
        if (!File(Constants.SETTINGS_FILE).exists()) {
            Log.d("ManageAssets", "Copying resources to ${Constants.SETTINGS_FILE}")
            assetCopier.copy("libopenmw/openmw/settings.fallback.cfg", Constants.SETTINGS_FILE)
        }

        // copy fallback openmw_base.cfg
        if (!File(Constants.OPENMW_BASE_CFG).exists()) {
            Log.d("ManageAssets", "Copying base config to ${Constants.OPENMW_BASE_CFG}")
            assetCopier.copy("libopenmw/openmw/openmw.base.cfg", Constants.OPENMW_BASE_CFG)
        }

        // copy fallback openmw_fallback.cfg
        if (!File(Constants.OPENMW_FALLBACK_CFG).exists()) {
            Log.d("ManageAssets", "Copying fallback config to ${Constants.OPENMW_FALLBACK_CFG}")
            assetCopier.copy("libopenmw/openmw/openmw.fallback.cfg", Constants.OPENMW_FALLBACK_CFG)
        }
    }

    fun createUI() {
        if (!File(Constants.USER_UI).isDirectory) {
            // Create USER_UI directory if it doesn't exist
            File(Constants.USER_UI).mkdirs()
            val src = File(Constants.UI)
            val dst = File(Constants.USER_UI)
            dst.mkdirs()
            src.copyRecursively(dst, true)
        }

        // Copy input_v3.xml to USER_CONFIG directory
        val inputFile = File(Constants.USER_UI, "input_v3.xml")
        val configFolder = File(Constants.USER_CONFIG)

        configFolder.mkdirs() // Ensure USER_CONFIG directory exists
        val configFile = File(configFolder, "input_v3.xml")

        // Copy file only if configFile does not exist
        if (!configFile.exists()) {
            inputFile.copyTo(configFile, overwrite = false)
        }
    }

    fun resetUI() {
        val uiDir = File(Constants.USER_UI)

        // Recursively delete all files and subdirectories in the uiDir
        uiDir.deleteRecursively()

        // Check if the directory still exists and delete it
        if (uiDir.exists()) {
            uiDir.delete()
        }

        // Create a new UI configuration
        createUI()
    }

    fun installUQMResourceFiles() {
        File(Constants.USER_CONFIG).mkdirs()
        if (!File(Constants.USER_FILE_STORAGE + "/resources/libuqm").isDirectory) {
            assetCopier.copy("libuqm", Constants.RESOURCES + "/libuqm")


        }
    }

    /**
     * Reset user resource files to default
     */
    fun removeResourceFiles() {
        reinstallStaticFiles()
        deleteRecursive(File(Constants.USER_FILE_STORAGE + "/resources/"))

        val src = File(Constants.RESOURCES)
        val dst = File(Constants.USER_FILE_STORAGE + "/resources/")
        dst.mkdirs()
        src.copyRecursively(dst, true)
    }

    /**
     * Resets user config to default values by removing it
     */
    fun removeUserConfig() {
        deleteRecursive(File(Constants.USER_CONFIG))
        File(Constants.USER_FILE_STORAGE + "/config").mkdirs()
        File(Constants.USER_OPENMW_CFG).writeText("# This is the user openmw.cfg. Feel free to modify it as you wish.\n")
    }
}


package org.openmw.utils

import android.content.Context
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.openmw.Constants
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter

@Composable
fun S3LightFixes() {
    val context = LocalContext.current
    var output by remember { mutableStateOf("") }
    var inputCommand by remember { mutableStateOf("") }
    val scrollState = rememberScrollState()
    val newFeatureEnabledChecked by GameFilesPreferences.loadNewFeatureEnabledState(context).collectAsState(initial = false)

    LaunchedEffect(output) {
        scrollState.animateScrollTo(scrollState.maxValue)
    }

    Column(modifier = Modifier.padding(2.dp)) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Button(onClick = {
                CoroutineScope(Dispatchers.IO).launch {
                    s3LightFixesCMD(context) { commandOutput ->
                        withContext(Dispatchers.Main) {
                            output = output.plus(commandOutput).plus("\n")
                        }
                    }
                }
            }) {
                Text("Run S3LightFixes")
            }
            if (newFeatureEnabledChecked) {
                Button(onClick = {
                    CoroutineScope(Dispatchers.IO).launch {
                        shellExec2(context, inputCommand) { commandOutput ->
                            withContext(Dispatchers.Main) {
                                output = output.plus(commandOutput).plus("\n")
                            }
                        }
                    }
                }) {
                    Text("Custom")
                }
            }
        }
        if (newFeatureEnabledChecked) {
            Spacer(modifier = Modifier.height(8.dp))
            TextField(
                value = inputCommand,
                onValueChange = { inputCommand = it },
                label = { Text("Enter command") },
                modifier = Modifier.fillMaxWidth()
            )
        }
        Spacer(modifier = Modifier.height(16.dp))
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .background(Color.Black)
                .padding(8.dp)
                .verticalScroll(scrollState)
        ) {
            Text(
                text = output,
                color = Color.White,
                fontSize = 12.sp,
                modifier = Modifier.padding(4.dp)
            )
        }
    }
}

fun s3LightFixesCMD(context: Context, onOutputUpdate: suspend (String) -> Unit) {
    val overrideFolder = File(Constants.USER_FILE_STORAGE, "OpenMW/Override")
    val commands = listOf(
        "OPENMW_CONFIG=\"${Constants.USER_OPENMW_CFG}\" S3L_DEBUG=1 S3L_NO_NOTIFICATIONS=1 ./libS3LightFixes.so -o $overrideFolder"
    )
    appendFallbackArchives(context)
    CoroutineScope(Dispatchers.IO).launch {
        // Execute each command sequentially with real-time updates
        for (command in commands) {
            shellExec2(context, command) { outputLine ->
                withContext(Dispatchers.Main) {
                    onOutputUpdate(outputLine)
                }
            }
        }
    }
}

suspend fun shellExec2(context: Context, cmd: String, onOutputUpdate: suspend (String) -> Unit): String {
    val output = StringBuilder()
    val workingDir = context.applicationInfo.nativeLibraryDir
    val overrideFolder = File(Constants.USER_FILE_STORAGE, "OpenMW/Override")

    // Ensure there's at least one empty line at the bottom of Constants.USER_OPENMW_CFG
    val userOpenMWCfgFile = File(Constants.USER_OPENMW_CFG)
    if (userOpenMWCfgFile.exists()) {
        val content = userOpenMWCfgFile.readText()
        if (!content.endsWith("\n")) {
            userOpenMWCfgFile.appendText("\n")
        }
    }

    try {
        val processBuilder = ProcessBuilder().apply {
            directory(File(workingDir))
            command("/system/bin/sh", "-c",
                """
            export OPENMW_OVERRIDE_DIR=$overrideFolder;
            export USER_OPENMW_CFG=${Constants.USER_OPENMW_CFG};
            export OPENMW_CFG=${Constants.OPENMW_CFG};
            export OPENMW_BASE_CFG=${Constants.OPENMW_BASE_CFG};
            export OPENMW_FALLBACK_CFG=${Constants.OPENMW_FALLBACK_CFG};
            $cmd
            """.trimIndent())
            redirectErrorStream(true)
        }

        val process = withContext(Dispatchers.IO) {
            processBuilder.start()
        }
        val reader = process.inputStream.bufferedReader()

        var line: String?
        while (withContext(Dispatchers.IO) {
                reader.readLine()
            }.also { line = it } != null) {
            withContext(Dispatchers.Main) {
                onOutputUpdate(line ?: "")
            }
            output.append(line).append("\n")
        }

        withContext(Dispatchers.IO) {
            process.waitFor()
        }
    } catch (e: Exception) {
        val sw = StringWriter()
        val pw = PrintWriter(sw)
        e.printStackTrace(pw)
        val errorOutput = "Error executing command: ${e.message}\nStacktrace:\n$sw"
        withContext(Dispatchers.Main) {
            onOutputUpdate(errorOutput)
        }
        output.append(errorOutput)
    }

    return output.toString()
}

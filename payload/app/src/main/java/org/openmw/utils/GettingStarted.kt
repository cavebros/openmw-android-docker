@file:OptIn(InternalCoroutinesApi::class)

package org.openmw.utils

import android.content.Intent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch
import org.openmw.EngineActivity
import org.openmw.ui.controls.UIStateManager.configureControls
import org.openmw.ui.controls.UIStateManager.highlightStep
import org.openmw.utils.GameFilesPreferences.setTutorial
import org.openmw.utils.GameFilesPreferences.setWhatsNew

@Composable
fun MyAlertDialog(
    showDialog: MutableState<Boolean>
) {
    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val whatsNew by GameFilesPreferences.getWhatsNew(context).collectAsState(initial = false)
    val tutorial by GameFilesPreferences.getTutorial(context).collectAsState(initial = false)
    val isChecked = whatsNew && tutorial
    if (showDialog.value) {
        AlertDialog(
            onDismissRequest = {
                // Close the dialog when the user touches outside or presses the back button
                showDialog.value = false
            },
            title = {
                Text(text = "Welcome to Alpha-3 Launcher!")
            },
            text = {
                Column {
                    Text("Getting Started.")
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Column(
                            verticalArrangement = Arrangement.Center,
                            horizontalAlignment = Alignment.Start,
                            modifier = Modifier.weight(1f)
                        ) {
                            Text(
                                text = "Close and don't show again",
                                color = Color.White
                            )
                        }
                        Switch(
                            checked = !isChecked,
                            onCheckedChange = { checked ->
                                scope.launch {
                                    setWhatsNew(context, !checked)
                                    setTutorial(context, !checked)
                                }
                            }
                        )
                    }
                    Spacer(modifier = Modifier.height(8.dp))
                    Button(
                        onClick = {
                            configureControls = true
                            highlightStep = 1

                            scope.launch {
                                setTutorial(context, true)
                            }
                            val intent = Intent(context, EngineActivity::class.java).apply {
                                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            }
                            context.startActivity(intent)
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(56.dp),
                        shape = RectangleShape,
                        colors = ButtonDefaults.buttonColors(
                            containerColor = Color.Black
                        )
                    ) {
                        Text("Configure Controls Tutorial", color = Color.White)
                    }
                }
            },
            confirmButton = {
            },
            dismissButton = {
                Button(
                    onClick = {
                        showDialog.value = false
                    }
                ) {
                    Text("close.")
                }
            }
        )
    }
}

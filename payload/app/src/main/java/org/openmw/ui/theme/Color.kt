package org.openmw.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)
val Black = Color.Black
val White = Color.White

val gradientColors = listOf(
    Color(0xFF42A5F5),
    Color(0xFF478DE0),
    Color(0xFF3F76D2),
    Color(0xFF3B5FBA)
)

val redGradientColors = listOf(
    Color(0xFFEF5350),
    Color(0xFFE57373),
    Color(0xFFEF9A9A),
    Color(0xFFFFCDD2)
)

val greenGradientColors = listOf(
    Color(0xFF66BB6A),
    Color(0xFF81C784),
    Color(0xFFA5D6A7),
    Color(0xFFC8E6C9)
)

val yellowGradientColors = listOf(
    Color(0xFFFFEB3B),
    Color(0xFFFFF176),
    Color(0xFFFFF59D),
    Color(0xFFFFF9C4)
)

val purpleGradientColors = listOf(
    Color(0xFFAB47BC),
    Color(0xFFBA68C8),
    Color(0xFFCE93D8),
    Color(0xFFE1BEE7)
)

val orangeGradientColors = listOf(
    Color(0xFFFFA726),
    Color(0xFFFFB74D),
    Color(0xFFFFCC80),
    Color(0xFFFFE0B2)
)

val pinkGradientColors = listOf(
    Color(0xFFF06292),
    Color(0xFFF48FB1),
    Color(0xFFF8BBD0),
    Color(0xFFFCE4EC)
)

val tealGradientColors = listOf(
    Color(0xFF26A69A),
    Color(0xFF4DB6AC),
    Color(0xFF80CBC4),
    Color(0xFFB2DFDB)
)

val brownGradientColors = listOf(
    Color(0xFF8D6E63),
    Color(0xFFA1887F),
    Color(0xFFBCAAA4),
    Color(0xFFD7CCC8)
)
